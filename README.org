Very WIP.

You need to use nightly Rust to build this project:
#+BEGIN_SRC sh
rustup override set nightly
#+END_SRC

(if you don't have =rustup= installed, go [[https://rustup.rs/][here]])

To download dependencies, build, and run:
#+BEGIN_SRC sh
cargo run
#+END_SRC
